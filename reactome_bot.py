__author__ = 'Sarah Keating'

from wikidataintegrator import wdi_core, wdi_property_store, wdi_helpers
import add_entry
import global_variables
from time import gmtime, strftime
import time
from SPARQLWrapper import SPARQLWrapper, JSON
import simplejson
import display_item
import os, sys


class ReactomeBot:
    def __init__(self, write_to_wd):
        """
        Class to run the Reactome Bot
        """
        self.writing_to_WD = write_to_wd
        self.fast_run = False

        # variables for the wikidata run
        self.logincreds = None
        self.fast_run_base_filter = dict({'P3937': '', 'P703': 'Q15978631'})
        self.wikidata_sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")
        self.wikidata_sparql.setReturnFormat(JSON)

        self.current_species = 'Q15978631'  # Homo Sapiens

        wdi_core.WDItemEngine.setup_logging(log_name="WD_bot_run-{}.log".format(
            time.strftime('%Y%m%d_%H_%M', time.localtime())))

        # tell the properties about reactome ID
        wdi_property_store.wd_properties['P3937'] = {
            'datatype': 'string',
            'name': 'Reactome ID',
            'domain': ['pathways'],
            'core_id': True
        }

    ###############################################################
    # functions to set class values

    def set_fast_run(self, fast):
        """
            sets the fast run flag
        :param fast: boolean for fast run flag
        """
        self.fast_run = fast

    def set_logincreds(self, logincreds):
        """
        Sets the wikidata login credentials
        :param logincreds:
        """
        self.logincreds = logincreds

    # future proofing functions to allow code to set species
    def set_species(self, index):
        """
        Sets the species being used for import
        :param index: integer index in supported list
        """
        self.current_species = global_variables.supported_species[index]['WDItem']

    ############################################################################
    #  Functions for main entries

    @staticmethod
    def create_reference(result):
        """
        Function to create the reference that is added to many properties
        tailored for Reactome
        :param result: the single result read from Reactome
        :return: reference: list of entries for the reference section

        """
        ref_stated_in = wdi_core.WDItemID(value="Q2134522", prop_nr='P248', is_reference=True)
        str_time_now = strftime("+%Y-%m-%dT00:00:00Z", gmtime())
        ref_retrieved = wdi_core.WDTime(str_time_now, prop_nr='P813', is_reference=True)
        ref_reactome = wdi_core.WDString(result['pwId']['value'], prop_nr='P3937', is_reference=True)
        if ref_reactome is None:
            reference = [ref_stated_in, ref_retrieved]
        else:
            reference = [ref_stated_in, ref_retrieved, ref_reactome]
        return reference

    def create_or_update_items(self, results, data_type, additional_loop):
        """
        Function to loop through all results and create or update a wikidata item
        :param results: the dictionary of results read from Reactome
        :param data_type: string indication type of entry e.g. 'pathway', 'reaction'
        :param additional_loop: boolean indication whether to run all the results of just
                                ones that need repeating
        :return: None
        """
        if results is None:
            return
        i = 0
        length_progress_bar = 1
        if not additional_loop:
            if len(results["results"]["bindings"]) > 0:
                length_progress_bar = len(results["results"]["bindings"])
        else:
            if len(global_variables.used_wd_ids['repeat']) > 0:
                length_progress_bar = len(global_variables.used_wd_ids['repeat'])
        if length_progress_bar == 0:
            self.print_progress_bar(1, 1)
            return

        self.print_progress_bar(i, length_progress_bar)
        for result in results["results"]["bindings"]:
            # if we are looping only process entries that need repeating
            if additional_loop:
                term = result['pwId']['value']
                if term not in global_variables.used_wd_ids['repeat']:
                    continue
                else:
                    global_variables.used_wd_ids['repeat'].remove(term)
                    if term in global_variables.repeat_needed[data_type]:
                        global_variables.repeat_needed[data_type].remove(term)

            i += 1
            property_list = dict()
            if result['pwLabel']['value'] == '':
                self.print_progress_bar(i, length_progress_bar)
                continue
            self.create_or_update_item(result, property_list, data_type)
            self.print_progress_bar(i, length_progress_bar)

    def create_or_update_item(self, result, property_list, data_type):
        """
        Function to create or update a pathway item in wikidata
        :param result: a single result entry for a Reactome Pathway
        :param property_list: the list of wikidata properties to be populated
        :return: None
        """
        if result['pwLabel']['value'] == '':
                return False
        reference = self.create_reference(result)
        if data_type == 'pathway':
            # print('Creating/updating pathway: ' + result["pwId"]["value"])
            add_pathway = add_entry.AddPathway(result['pwId']['value'], self.wikidata_sparql, reference,
                                               self.current_species)
            add_pathway.add_pathway(property_list, result)
        elif data_type == 'entity':
            # print('Creating/updating entity: ' + result["pwId"]["value"])
            add_entity = add_entry.AddEntity(result['pwId']['value'], self.wikidata_sparql, reference,
                                             self.current_species)
            add_entity.add_entity(property_list, result)
        elif data_type == 'reaction':
            # print('Creating/updating reaction: ' + result["pwId"]["value"])
            add_entity = add_entry.AddReaction(result['pwId']['value'], self.wikidata_sparql, reference,
                                               self.current_species)
            add_entity.add_reaction(property_list, result)
        elif data_type == 'modprot':
            # print('Creating/updating modified protein: ' + result["pwId"]["value"])
            add_entity = add_entry.AddModProt(result['pwId']['value'], self.wikidata_sparql, reference,
                                              self.current_species)
            add_entity.add_modprot(property_list, result)
        self.write_to_wikidata(property_list, result)

    def write_to_wikidata(self, property_list, result, info=False):
        """
        Function to write the wikidata item
        :param property_list: the properties to be used for this item
        :param result: a single entry result from Reactome
        :return:
        """
        data2add = []
        for key in property_list.keys():
            for statement in property_list[key]:
                data2add.append(statement)
        # turn off messages from WDIntegrator
        # errors will be logged but nothing will print to console
        sys.stdout = open(os.devnull, 'w')
        if not info:
            wdpage = wdi_core.WDItemEngine(item_name=result["pwLabel"]["value"], data=data2add,
                                           server=global_variables.server,
                                           domain="pathway", fast_run=self.fast_run,
                                           fast_run_base_filter=self.fast_run_base_filter)
        else:
            wdpage = wdi_core.WDItemEngine(data=data2add,
                                           wd_item_id="Q2134522",
                                           append_value="P577",
                                           server=global_variables.server,
                                           domain="database", fast_run=True,
                                           fast_run_base_filter=dict({'P4793': ''}))

        if not info:
            wdpage.set_label(result["pwLabel"]["value"])
            wdpage.set_description(result['pwDescription']['value'])

        if self.writing_to_WD and self.logincreds:
            item_id_value = 0
            try:
                item_id_value = wdpage.write(self.logincreds)

                if item_id_value != 0:
                    if item_id_value not in global_variables.edited_wd_pages:
                        global_variables.edited_wd_pages.append(item_id_value)
#                    print('https://www.wikidata.org/wiki/{0}'.format(item_id_value))
            except Exception as e:
                try:
                    wdi_core.WDItemEngine.log("ERROR", wdi_helpers.format_msg(result['pwId']['value'], 'P3937',
                                                                              None, str(e), type(e)))
                    ex_error = e.args[0]['error']['info'].replace(',', ':')
                    global_variables.exceptions.append(ex_error)
                except simplejson.JSONDecodeError as je:
                    wdi_core.WDItemEngine.log("ERROR", wdi_helpers.format_msg(result['pwId']['value'], 'P3937',
                                                                              None, str(je), type(je)))
                    global_variables.exceptions.append(str(je).replace(',', ':'))
                finally:
                    global_variables.exceptions.append('Unknown exception')

        else:
            #     # display the items that would have been entered
            #     d = display_item.DisplayItem(wdpage.get_wd_json_representation(), global_variables.server)
            #     d.show_item()
            if not info:
                print('{0}'.format(result['pwLabel']['value']))
            else:
                print('added to Reactome global page')

        # turn write to console back on
        sys.stdout = sys.__stdout__

    ##########################################################################
    # Function to add version information to main Reactome page
    def add_release_info(self, release_info):
        """
        Function to add a publication date entry to the main wikidata Reactome entry page

        :param release_info: dictionary of information about release (no/date etc)

        :return: No return
        """
        url = 'https://reactome.org/about/news/{0}-version-{1}-released'\
            .format(release_info['news_item_no'], release_info['releaseNo'])
        reference = [wdi_core.WDUrl(url, prop_nr='P854', is_reference=True)]
        add_release = add_entry.AddEntry('', self.wikidata_sparql, reference, '')
        property_list = dict()
        add_release.add_release_info(property_list, release_info)
        self.write_to_wikidata(property_list, None, True)

    ##########################################################################
    # Function to output reports

    @staticmethod
    def output_report(data_type):
        """
        Function to write a report detailing any missing wikidata entries
        Creates a file logs/wikidata_update_{time}.csv
        :return:
        """
        if not os.path.exists('logs'):
            os.makedirs('logs')
        timestringnow = gmtime()
        filename = 'logs/wikidata_update_{0}-{1}-{2}-{3}.csv'.format(timestringnow[0],
                                                                     timestringnow[1], timestringnow[2], data_type)
        f = open(filename, 'w')
        f.write('wikidata entries,')
        for term in global_variables.edited_wd_pages:
            f.write('{0},'.format(term))
        f.write('\n')
        f.write('missing pmids,')
        for pmid in global_variables.used_wd_ids['pmid']:
            f.write('{0},'.format(pmid))
        f.write('\n')
        f.write('missing go terms,')
        for term in global_variables.used_wd_ids['goterms']:
            f.write('{0},'.format(term))
        f.write('\n')
        f.write('missing proteins,')
        for term in global_variables.used_wd_ids['proteins']:
            f.write('{0},'.format(term))
        f.write('\n')
        f.write('missing chebi,')
        for term in global_variables.used_wd_ids['chebi']:
            f.write('{0},'.format(term))
        f.write('\n')
        f.write('missing reactome links,')
        if data_type == "final":
            for react in global_variables.missing_reactome:
                f.write('{0},'.format(react))
        else:
            for react in global_variables.used_wd_ids['reactome']:
                f.write('{0},'.format(react))
        f.write('\n')
        # do not write out exceptions as these are logged by WDIntegrator
        # f.write('exceptions,')
        # for term in global_variables.exceptions:
        #     f.write('{0},'.format(term))
        # f.write('\n')
        f.close()

    # Print iterations progress
    def print_progress_bar (self, count, total, length=100, fill='='):
        """
        Call in a loop to create terminal progress bar
        @params:
            count   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
        """
        percent = round(100.0 * (count/total), 0)
        filled_length = int(round(length * count/total))
        bar = fill * filled_length + '-' * (length - filled_length)
        sys.stdout.write('\r[{0}] {1}{2} {3}\r'.format(bar, percent, '%', ''))
        sys.stdout.flush()
        # # Print New Line on Complete
        if count == total:
            print()
