
# The wikidata\_update\_DATE-final.csv file #

An update summary file is written in the logs directory.

- wikidata\_update\_DATE-final.csv


where DATE is the date the code was run.

This takes the following format:


```console
wikidata entries,Q2134522
missing pmids,2165567
missing go terms,
missing proteins,ENSG00000124762
missing chebi,38574
missing reactome links,R-HSA-8852348
```

### Wikidata entries

This is a list of entries made or updated during the import. They can be accessed by substituting the term in the following url: https://www.wikidata.org/wiki/Qnnnnn
 

### Missing pmids

Pub med entries can be made to Wikidata using the service at https://tools.wmflabs.org/fatameh



### Missing GO terms/proteins/chebi ids

Often for missing items like these, the issue is that there are inconsistencies in cross-references, or item were incorrectly merged, and the items need to be manually sorted out. Ideally, there would be a place for items with these kinds of issues to get posted and people can work on sorting them out, however this doesn't really exist. 

Currently the suggestion is to post to:

https://www.wikidata.org/wiki/Wikidata_talk:WikiProject_Molecular_biology

and also in an issue on 

https://github.com/SuLab/GeneWikiCentral/issues

Note: Uniprot identifiers starting ENG have not yet been entered in Wikidata. They are aware of these.

### Missing Reactome links

A successful run should mean that there are no missing Reactome links. However, because the data is inter-related, occasionally a link is logged as missing and then entered at a later point. The bot does check for missing links at the end of each stage and will loop again if necessary. 


-----
This file was last updated in July 2018. 