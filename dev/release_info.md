
# The release\_info.txt file #

```console
uname,Pathwaybot,     # wikidata account name
password,password, # ADD account password
inputdir,input,       # UPDATE
releaseNo,64,         # UPDATE
news_item_no,102,     # UPDATE
day,26,               # UPDATE - must be 2 digits
month,03,             # UPDATE - must be 2 digits
year,2018,            # UPDATE - must be 4 digits
```

uname - the wikidata user account name.Pathwaybot is the user account verified to run this bot

password - the password for the Pathwaybot account

NOTE: The bot code must be run from this account.

inputdir - the directory under the r-wikidata-bot directory where the input .csv files have been placed

releaseNo - the Reactome release number

news_item_no - part of the URL from Reactome News that identifies the release: e.g. in https://reactome.org/about/news/103-version-65-released it would be '103'

day - release day (note add 0 if a single number)

month - release month (note add 0 is a single number)

year - release year (4 digits)

-----
This file was last updated in July 2018. 