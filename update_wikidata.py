__author__ = 'Sarah Keating'

from wikidataintegrator import wdi_login
import sys
import os
import reactome_bot
import global_variables
import format_reactome_data


def check_settings(uname):
    """
    Function just to double check we want to write data to wikidata
    :param uname: the username supplied

    :return: [proceed, writeWD, fast] depending on whether to proceed and what settings to use

    NOTE: not included in automated run
    """
    if uname == 'SarahKeating':
        print('Using skeating account')
    else:
        print('Using bot account')
    writewd = True
    fast = True
    writing_to_wd = input('write to wikidata (Y):')
    if writing_to_wd == 'Y':
        fastvar = input('using fastrun (Y):')
        if fastvar != 'Y':
            fast = False
        var = input('Proceed (Y):')
    else:
        writewd = False
        var = 'Y'
    if var == 'Y':
        return [True, writewd, fast]
    else:
        return [False, writewd, fast]


def populate_release_info(release_info, line):
    """
    Function to create the release info dictionary from the file

    :param release_info: dictionary to populate
    :param line: line from file

    :return: No return
    """
    if not len(line) == 0:
        parts = line.split(',')
        release_info[parts[0]] = parts[1]


def write_data_from_file(data_type, inputdir, bot, second_run=False):
    """
        Write data for the given datatype

    :param data_type: String representing the datatype ['modprot', 'entity', 'reaction', 'pathway']
    :param inputdir: input directory containing csv files from wikidata-exporter
    :param bot: instance of the ReactomeBot
    :param second_run: a final loop to catch inter-type dependencies

    :return: No return
    """
    data = format_reactome_data.ReactomeData('HSA', data_type, inputdir)
    results = data.get_data_from_reactome()
    if not results:
        print('data file missing for {0}'.format('data_type'))
        sys.exit()
    # because we are creating cyclic relationships sometimes on the first run an entry is not yet
    # present so we check and do another run
    # put in a count so we dont endlessly cycle
    done = False
    count = 0
    maxloops = 2
    additional_loop = False
    global_variables.used_wd_ids['reactome'].clear()
    global_variables.used_wd_ids['repeat'].clear()
    if second_run:
        maxloops = 1
        additional_loop = True
        for term in global_variables.repeat_needed[data_type]:
            global_variables.used_wd_ids['repeat'].append(term)

    while not done and count < maxloops:
        print('Writing data for {0} loop {1} of a possible {2} loops'.format(data_type, count+1, maxloops))
        bot.create_or_update_items(results, data_type, additional_loop)
        count += 1
        if len(global_variables.used_wd_ids['reactome']) == 0:
            done = True
        elif count < maxloops:
            global_variables.used_wd_ids['reactome'].clear()
            additional_loop = True
    if len(global_variables.used_wd_ids['reactome']) > 0:
        # record entries that may need further repetition
        for term in global_variables.used_wd_ids['repeat']:
            global_variables.repeat_needed[data_type].append(term)


def main(args):
    """Usage: update_wikidata (release_info.txt optional)

       This program assumes the presence of a file named: release_info.txt that contains
       the information for the release. If a second argument is passed it is assumed to be the
       name of the release information file.

       This program take the inputdir containing csv files generated by
       wikidata-exporter and writes/updates the wikidata pages
       It uses the an array of numbers [Reactome version number, newsupdate number, year, month, day] to add the
       new publication information to main Reactome
       entry in wikidata https://www.wikidata.org/wiki/Q2134522
    """
    rel_file = 'release_info.txt'
    release_info = dict()
    if len(args) > 2:
        print(main.__doc__)
        sys.exit()
    elif len(args) == 2:
        rel_file = args[1]
        if not os.path.isfile(rel_file):
            print(main.__doc__)
            print('The release information file {0} is missing'.format(rel_file))
            sys.exit()
    elif len(args) == 1 and not os.path.isfile(rel_file):
            print(main.__doc__)
            print('The release information file {0} is missing'.format(rel_file))
            sys.exit()

    # read in release_info
    f = open(rel_file, 'r')
    for line in f.readlines():
        populate_release_info(release_info, line)
    f.close()

    # check input directory exists
    if not os.path.isdir(release_info['inputdir']):
        print('The input directory specified does not exist or cannot be found')
        sys.exit()

    data_type_all = ['modprot', 'entity', 'reaction', 'pathway']

    # used for testing but should both be true
    writewd = True
    fast = True
    bot = reactome_bot.ReactomeBot(writewd)
    bot.set_fast_run(fast)
    server = global_variables.server
    try:
        logincreds = wdi_login.WDLogin(user=release_info['uname'], pwd=release_info['password'], server=server)
        bot.set_logincreds(logincreds)
    except Exception as e:
        print('Error logging into wikidata: {0}'.format(e.args[0]))
        sys.exit()
    print('Updating wikidata for version {0}'.format(release_info['releaseNo']))
    bot.add_release_info(release_info)
    print('First pass of two:')
    for i in range(0, 4):
        write_data_from_file(data_type_all[i], release_info['inputdir'], bot)
    #    bot.output_report(data_type_all[i])
        print('First pass of {0} completed'.format(data_type_all[i]))
    print('Checking for missing entries')
    # create a loop over any repeat needed entries
    for i in range(0, 4):
        if len(global_variables.repeat_needed[data_type_all[i]]) > 0:
            write_data_from_file(data_type_all[i], release_info['inputdir'], bot, second_run=True)
    #        bot.output_report(data_type_all[i])
            print('Second pass of {0} completed'.format(data_type_all[i]))
            for term in global_variables.used_wd_ids['reactome']:
                global_variables.missing_reactome.append(term)
    print('Update complete')
    bot.output_report('final')

if __name__ == '__main__':
    main(sys.argv)
